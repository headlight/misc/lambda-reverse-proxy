# Reverse proxy

### Dependencies
- [Node.js](https://nodejs.org/)
- [Serverless](https://serverless.com)

---
### Config
Create a config.yml with `TARGET_HOST`, `TARGET_PORT` and `ENV` variables set.

`TARGET_HOST` is required, however if `TARGET_PORT` isn't set at all it will default to `80` and `ENV` defaults to `dev`.

---

Installation
```
$ npm i -g serverless
```

Deployment
```
$ serverless deploy
```

Destroy
```
$ serverless remove
```
