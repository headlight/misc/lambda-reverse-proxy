'use strict';
const http = require('http');

const proxy = (event, context, callback) => {
  
  const proxyParams = {
    host: process.env.TARGET_HOST,
    port: process.env.TARGET_PORT,
    path: event.path,
  };
  
  const req = http.request(proxyParams, function(res) {
    
    let data = '';

    res.setEncoding('utf8');

    res.on('data', function(chunk) {
      data += chunk;
    });
    
    res.on('end', function() {
      
      callback(null, {
        statusCode: res.statusCode,
        headers: res.headers,
        body: data,
      });

    });

  });

  req.end();

};

module.exports = {
  proxy,
};
